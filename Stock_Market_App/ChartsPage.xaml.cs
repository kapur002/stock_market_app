﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

//import Models folder, used for JSON and formatting
using StockDataNamespace;
using DayModel;
using stockListGlobals;

//import nuget packages 
//call MicroCharts.Form nuget package and call the class Entry
using Entry = Microcharts.Entry;
using SkiaSharp;
using Microcharts;

namespace Stock_Market_App
{
    public partial class ChartsPage : ContentPage
    {

        //declare variables needed for the ChartsPage class
        float tempStockPriceHolder;
        List<Entry> listEntries = new List<Entry> { };
        List<Entry> listEntries2 = new List<Entry> { };

        Entry entryObject;

        //function to fill the chart
        public void fillChart()
        {
            int START = 0;
            int ZERO = 0;
            int DIVISIBLE_BY_TWO = 2;

            //make sure that we get the stock prices
            if (stockListGlobal.stockRetrieved == true)
            {
                for (int index = START; index < stockListGlobal.dailyStockList.Count; index++)
                {
                    //if condition to make sure chart only captures nearest stock prices ending in zero
                    if (index % DIVISIBLE_BY_TWO == ZERO) 
                    {

                        //capture the stock price in a temporary variable
                        tempStockPriceHolder = float.Parse(stockListGlobal.dailyStockList[index].The2High);

                        //call Entry class constructor, pass thru the tempStockPrice into constructor
                        //and assign memory location to entryObject, which is an object of Entry class
                        entryObject = new Entry(tempStockPriceHolder)
                        {
                            Color = SKColor.Parse("#127ac9"),
                            Label = "",
                            ValueLabel = stockListGlobal.dailyStockList[index].The2High,
                        };

                        listEntries.Add(entryObject);
                        listEntries.Reverse();
                    }
                    else
                    {
                        entryObject = new Entry(tempStockPriceHolder)
                        {
                            Color = SKColor.Parse("#127ac9"),
                        };
                        listEntries2.Add(entryObject);
                    }
                }
            }
        }

        public ChartsPage()
        {
            InitializeComponent();

            listEntries.Clear();
            fillChart();
            ChartView.Chart = new LineChart { Entries = listEntries };

            //clearing second chart
            listEntries2.Clear();
            fillChart();
            Chart2.Chart = new LineChart { Entries = listEntries2 };

        }

        void Handle_Appearing(object sender, System.EventArgs e)
        {
            listEntries.Clear();
            fillChart();
            ChartView.Chart = new LineChart { Entries = listEntries };

            listEntries.Clear();

            listEntries2.Clear();
            Chart2.Chart = new LineChart { Entries = listEntries2 };
            fillChart();

        }

        void generateChart(object sender, EventArgs e)
        {
            //clearing first chart
            listEntries.Clear();


            ChartView.Chart = new LineChart { Entries = listEntries };

            fillChart();
            ChartView.Chart = new LineChart { Entries = listEntries };


        }

        void generateSecondChart(object sender, EventArgs e)
        {
            //clearing second chart
            listEntries2.Clear();

            Chart2.Chart = new LineChart { Entries = listEntries2 };
            fillChart();
        }
    }
}
