﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;

//import Models
using StockDataNamespace;
using DayModel;
using stockListGlobals;


namespace Stock_Market_App
{
    public partial class StartPage : ContentPage
    {
        //initialize comparison price holders to their respective ceilings and floors
        double min = 99999.00; 
        double max = 00000.00;

        public string key = "QG7WLIXA15ZPDJVJ";  //personal API key
        DayData currentDay = new DayData();
            
        public StartPage()
        {
            InitializeComponent();
        }

        async void getStock(object sender, EventArgs e)
        {
            //capturing user selected stock
            string userSelectedStock = stockEntry.Text;

            //take the user selected stock and convert it into a URI
            string stock = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY" + "&symbol=" + userSelectedStock + "&apikey=" + key;
            Uri keyToURI = new Uri(stock);

            //creating a new object of HTTP Client
            HttpClient httpClientObject = new HttpClient();

            //create a variable to hold the response message of the http client object
            HttpResponseMessage clientResponse = await httpClientObject.GetAsync(keyToURI);

            //message containing the JSON information, read it as string and store in a string variable
            string jsonInfo = await clientResponse.Content.ReadAsStringAsync();

            var stockInfo = StockData.FromJson(jsonInfo);

            //initialize variables required for if else statements
            int START = 0;
            string EMPTY_STRING = "";
            string SINGLE_SPACE = " ";
            string STOCK_ERROR = stockListGlobal.ERROR;

            //Console.WriteLine(jsonInfo);
            if (jsonInfo != STOCK_ERROR)
            {
                //converting the dictionary into a list
                stockListGlobal.dailyStockList = stockInfo.TimeSeriesDaily.Values.ToList();
                stockListGlobal.datesStockList = stockInfo.TimeSeriesDaily.Keys.ToList();

                int DAILY_STOCK_COUNT = stockListGlobal.dailyStockList.Count;

                List<DayData> dayList = new List<DayData>();

                for (int index = START; index < DAILY_STOCK_COUNT; index++)
                {

                    currentDay.date = stockListGlobal.datesStockList[index];
                    currentDay.high = double.Parse(stockListGlobal.dailyStockList[index].The2High);
                    currentDay.low = double.Parse(stockListGlobal.dailyStockList[index].The3Low);
                    currentDay.close = double.Parse(stockListGlobal.dailyStockList[index].The4Close);

                    dayList.Add(currentDay);

                    currentDay = new DayData();

                }

                historyListView.ItemsSource = dayList;

                //set the stockRetrieved variable to true, as the stock has now been retrieved
                stockListGlobal.stockRetrieved = true;

                //call function to retrieve high and low values of stock
                getStockMinMax();
            }

            else if(jsonInfo == stockListGlobal.ERROR)
            {
                //WRITING TO CONSOLE TO SEE JSONINFO VARIABLE
                Console.WriteLine(jsonInfo);
                //DON'T THINK THIS WILL WORK, ATTEMPT TO DIFFERENTIATE BETWEEN ERRANEOUS INPUT
                if (jsonInfo == EMPTY_STRING || jsonInfo == SINGLE_SPACE)
                {
                    DisplayAlert("ERROR", "You have entered nothing!", "OK");
                }
                else
                {
                    DisplayAlert("ERROR", "Enter a valid stock symbol!", "Cancel");
                }

            }


        }

        void getStockMinMax()
        {

            int START = 0;

            //loop & compare each stock high and low
            for (int index = START; index < stockListGlobal.dailyStockList.Count; index++)
            {

                if (double.Parse(stockListGlobal.dailyStockList[index].The2High) >= max)
                {
                    max = double.Parse(stockListGlobal.dailyStockList[index].The2High);
                }

                if (double.Parse(stockListGlobal.dailyStockList[index].The3Low) <= min)
                {
                    min = double.Parse(stockListGlobal.dailyStockList[index].The3Low);
                }
            }

            //update labels to highs and lows
            lowestLbl.Text = min.ToString();
            highestLbl.Text = max.ToString();

            //reset values of min and max
            min = 99999.00;
            max = 00000.00;

        }

    }
}