﻿namespace stockListGlobals
{
    using StockDataNamespace;
    using System.Collections.Generic;

    public static class stockListGlobal
    {
        public static List<TimeSeriesDaily> dailyStockList;
        public static List<string> datesStockList;
        public static bool stockRetrieved = false;
        public static string ERROR = "{\n    \"Error Message\": \"Invalid API call. Please retry or visit the documentation (https://www.alphavantage.co/documentation/) for TIME_SERIES_DAILY.\"\n}";
    }
}

